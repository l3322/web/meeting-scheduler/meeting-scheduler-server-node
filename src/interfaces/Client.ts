import { Response } from 'express';

export interface Client {
  username:string;
  res: Response;
};

export interface Query {
  username:string;
};
