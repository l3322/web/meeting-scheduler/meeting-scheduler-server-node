export interface MeetingEventInfo {
  owner: string,
  name: string,
  local: string,
  availableTimes: Array<string>,
  deadline: string
}

export interface MeetingEvent {
  meetingInfo: MeetingEventInfo,
  dateVotes: Map<string, number>,
  clientVotes: Array<string>,
  clientWithoutVotes: Array<string>,
  isActive: boolean
}

export interface NewMeeting {
  owner: string,
  name: string,
  local: string,
  availableTimes: Array<string>,
  deadline: string
}

export interface MeetingInfoRequest {
  owner: string,
  name: string,
}

export interface MeetingInfoFeedback {
  owner: string,
  name: string,
}