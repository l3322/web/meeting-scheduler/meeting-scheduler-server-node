import express, { Request, Response } from 'express';
import cors from 'cors';
import { Client, Query } from './interfaces/Client';
import { MeetingEvent, MeetingEventInfo, NewMeeting } from './interfaces/Meeting';

const app = express();
const port = 4650; // TODO - Load it from .env file
var schedule = require('node-schedule');

app.use(express.json()); // Enable Express
app.use(cors()); // Enable CORS

let clients: Array<Client> = [] ;
let meetings: Array<MeetingEvent> = [];

const writeEvent = (res: Response, sseId: string, data: string) => {
  res.write(`id: ${sseId}\n`);
  res.write(`data: ${data}\n\n`);
};

const sendOkEvent = (_req: Request, res: Response) => {
  const sseId = new Date().toDateString();
  writeEvent(res, sseId, JSON.stringify({}));
};

const registerClient = (username: string, res: Response) => {
  res.writeHead(200, {
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive',
    'Content-Type': 'text/event-stream',
  });
  const newUser: Client = {
    username: username,
    res: res
  }
  clients.push(newUser)
  console.log('Added client ' + username)
}

app.get('/register', (req: Request, res: Response) => {
    if (req.headers.accept === 'text/event-stream') {
      // sendOkEvent(req, res);
      const { username } = req.query as unknown as Query;
      registerClient(username, res)
      sendOkEvent(req, clients[0].res);
    } else {
      res.json({ message: 'Ok' });
    }
});

app.post('/newmeeting', (req: Request, res: Response) => {
  const newMeeting: NewMeeting = req.body as NewMeeting;  
  const meetingInfo: MeetingEventInfo = addMeeting(newMeeting);
  meetingFeedbackEvent(meetingInfo)
});

function addMeeting(newMeeting: NewMeeting) : MeetingEventInfo {
  const meetingInfo: MeetingEventInfo = {
    owner: newMeeting.owner,
    name: newMeeting.name,
    local: newMeeting.local,
    availableTimes: [...newMeeting.availableTimes],
    deadline: newMeeting.deadline
  }

  let date : Map<string,number> = new Map();
  meetingInfo.availableTimes.forEach((time) => {
    date.set(time, 0)
  })
  let clientWithoutVotes: Array<string> = []
  clients.forEach((client) => {
    clientWithoutVotes.push(client.username)
  })

  const meetingEvent: MeetingEvent = {
    meetingInfo: meetingInfo,
    dateVotes: date,
    clientVotes: [],
    clientWithoutVotes: clientWithoutVotes,
    isActive: true
  }
  meetings.push(meetingEvent);

  // Schedule the deadline
  const [dateComponents, timeComponents] = newMeeting.deadline.split(' ');
  const [day, month, year] = dateComponents.split('/');
  const [hours, minutes] = timeComponents.split(':');

  const deadline = new Date(+year, +month - 1, +day, +hours, +minutes, 0);
  console.log('Deadline ' + deadline)
  var j = schedule.scheduleJob(deadline, async function(){
    console.log('deadline')
    endMeetingVotes(meetingEvent)
  });

  return meetingInfo;
}

function meetingFeedbackEvent(meetingInfo: MeetingEventInfo) {
  const sseId = new Date().toDateString();
  const meetingInfoFeedback = {
    id: 0,
    meetingInfo: meetingInfo
  }
  clients.forEach((client) => {
    writeEvent(client.res, sseId, JSON.stringify(meetingInfoFeedback));
  }) 
}

app.post('/vote', async (req: Request, res: Response) => {
  const meetingName: string = req.body.meetingName;
  const clientName: string = req.body.clientName;
  const voteList: Array<string> = req.body.voteList;
  await addVotes(meetingName, clientName, voteList);
});

const addVotes = async (meetingName: string, clientName: string, voteList: Array<string>) => {
  console.log('Vote from client ' + clientName)
  meetings.forEach((meeting) => {
    const clientVote = meeting.clientVotes.filter(client => client === clientName)
    if(meeting.meetingInfo.name === meetingName && clientVote.length === 0) {
      meeting.clientWithoutVotes = meeting.clientWithoutVotes.filter(client => client !== clientName);
      meeting.clientVotes.push(clientName)
      voteList.forEach((vote) => {
        const currentVotes = meeting.dateVotes.get(vote);
        if(currentVotes !== undefined)
          meeting.dateVotes.set(vote, currentVotes + 1);
      })
      voteFeedback(meetingName, clientName);
      if(meeting.clientWithoutVotes.length === 0) {
        setTimeout(() => endMeetingVotes(meeting), 1000);
      }
    }
  })
}

function voteFeedback(meetingName: string, clientName: string) {
  const meetingFeedback = {
    id: 2,
    meetingName: meetingName,
    vote: true
  }
  const sseId = new Date().toDateString();
  const client = clients.filter((value) => value.username === clientName)[0]
  writeEvent(client.res, sseId, JSON.stringify(meetingFeedback));
}

function replacer(key: any, value: any) {
  if(value instanceof Map) {
    return {
      value: Array.from(value.entries()) // or with spread: value: [...value]
    };
  } else {
    return value;
  }
}

function endMeetingVotes(meeting: MeetingEvent) {
  console.log('Meeting ended')
  console.log(meeting)
  meeting.isActive = false;
  const meetingFeedback = {
    id: 1,
    meeting: meeting
  }
  const sseId = new Date().toDateString();
  meeting.clientVotes.forEach((clientName) => {
    const client = clients.filter((value) => value.username === clientName)[0]
    writeEvent(client.res, sseId, JSON.stringify(meetingFeedback, replacer));
  })
  meeting.clientWithoutVotes.forEach((clientName) => {
    const client = clients.filter((value) => value.username === clientName)[0]
    writeEvent(client.res, sseId, JSON.stringify(meetingFeedback, replacer));
  })
}


app.listen(port, () => {
  console.log("Server running on port: ", port);
});