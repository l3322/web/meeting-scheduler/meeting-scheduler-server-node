
export const newMeetingValidation = (data: object) => {
  const Joi = require("@hapi/joi");
  const schema = Joi.object({
    owner: Joi.string().required(),
    name: Joi.string().required(),
    avaialableTimes: Joi.Date.required()
  });
  return schema.validate(data);
};